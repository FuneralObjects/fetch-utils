import Api from "./src/Api";
import ServiceApi from "./src/ServiceApi";

const FetchUtils = {
    Api,
    ServiceApi
};

export default FetchUtils;